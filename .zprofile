# If running from tty1 start sway
if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
  exec ~/.config/scripts/sway-nvidia-run
fi

if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 3 ]; then
  exec ~/.config/scripts/gnome-run
fi